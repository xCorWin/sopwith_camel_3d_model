zip $HOME/soppack/Fuse-B_1.25.zip F*B_1*.stl
zip $HOME/soppack/Fuse-BI_1.25.zip i/F*BI_1*.stl
zip $HOME/soppack/Gear_1.25.zip GEAR*.stl
zip $HOME/soppack/Wings_LI_1.25.zip i/W*LI_1*.stl
zip $HOME/soppack/Wings_L_1.25.zip W*L_1*.stl
zip $HOME/soppack/Wings_L_1.25.zip F7*.stl
zip $HOME/soppack/Struts_1.25.zip WSL*.stl
zip $HOME/soppack/Control_1.25.zip SAR*.stl
zip $HOME/soppack/Misc.zip ../Misc/*.stl
zip $HOME/soppack/Cura.zip ../Cura/*.curaprofile
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/LeftWingHollow-Ail.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/GearFull.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/Fuse-Hollow5.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/LeftWingHollow-Lower-0-1.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/Stabiliser.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/Aileron.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/Rudder1.stl $HOME/soppack
cp $HOME/Blender/Sopwith-Camel-New/os-post/Inputs/Elevator.stl $HOME/soppack
