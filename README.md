

These are output files for the STL required to print the Sopwith Camel 3d RC plane model.

See: https://sopwithcamel3dmodel.wordpress.com/
For Assemply and print instructions.
model is also on thingiverse: https://www.thingiverse.com/thing:3951370

the directories here refer to SCALE:
1 - 1/10
1.25 - 1/8
1.43 - 1/7 
etc

kind regards
--Cor

